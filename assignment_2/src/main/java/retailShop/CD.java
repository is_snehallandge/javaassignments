package retailShop;

public class CD implements TaxCalculator{

	int cdPrice;

	public int getCDPrice()
	{
		return cdPrice;
	}

	public int getCalculatedTax(int tax, int price) {
		int result = price*tax/100;
		return result;
		
	}
}
