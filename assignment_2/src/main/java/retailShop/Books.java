package retailShop;

public class Books implements TaxCalculator{

	int bookPrice;
	
	public int getBookPrice()
	{
		return bookPrice;
	}

	public int getCalculatedTax(int tax, int price) {
		return tax*price;
	}
}
