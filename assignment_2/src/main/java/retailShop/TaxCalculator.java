package retailShop;

public interface TaxCalculator {
	
	abstract int getCalculatedTax(int tax, int price);

}
