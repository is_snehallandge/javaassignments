package retailShop;

public class Cosmetics implements TaxCalculator {

	int cosmeticsPrice;

	public int getCDPrice()
	{
		return cosmeticsPrice;
	}

	public int getCalculatedTax(int tax, int price) {
		return price*tax/100;
	}
}
