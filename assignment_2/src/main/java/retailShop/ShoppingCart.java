package retailShop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShoppingCart extends Database {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		CD cd = new CD();
		Cosmetics cosmetics = new Cosmetics();

		ResultSet rs = null;
		int cdtax=0, cosmetictax = 0;
		int numberOfBooks, numberOfCds, numberOfCosmetics = 0;


		List<String> bookInputs = new ArrayList<String>();
		List<String> cdInputs = new ArrayList<String>();
		List<String> cosmeticsInput = new ArrayList<String>();

		Scanner scan  = new Scanner(System.in);

		System.out.println("Enter number of books : ");
		numberOfBooks = scan.nextInt();
		System.out.println("Enter name of book(s) : ");

		for(int i=0;i<numberOfBooks;i++){
			bookInputs.add(scan.next());
		}

		System.out.println("Enter number of CDs : ");
		numberOfCds = scan.nextInt();		
		System.out.println("Enter name of CD(s) : ");

		for(int i=0;i<numberOfCds;i++){
			cdInputs.add(scan.next());
		}

		System.out.println("Enter number of Cosmetics : ");
		numberOfCosmetics = scan.nextInt();		
		System.out.println("Enter name of cosmetics : ");

		for(int i=0;i<numberOfCosmetics;i++){
			cosmeticsInput.add(scan.next());
		}

		System.out.println("Item      Quantity	Cost");
		System.out.println("------------------------------------");
		for(int i=0;i<bookInputs.size();i++){
			rs = Database.getAllRecord(bookInputs.get(i));
			rs.next();
			System.out.println(rs.getString("bookName")+"	1	"+rs.getString("Price"));
		}

		for(int i=0;i<cdInputs.size();i++){
			rs = Database.getAllRecord(cdInputs.get(i));
			rs.next();
			System.out.println(rs.getString("bookName")+"	1	"+rs.getString("Price"));
			cdtax += cd.getCalculatedTax(10, rs.getInt("Price"));
		}

		for(int i=0;i<cosmeticsInput.size();i++){
			rs = Database.getAllRecord(cosmeticsInput.get(i));
			rs.next();
			System.out.println(rs.getString("bookName")+"	1	"+rs.getString("Price"));
			cosmetictax += cosmetics.getCalculatedTax(10, rs.getInt("Price"));
		}
		
		System.out.println("------------------------------------");
		System.out.println("Tax on CDs :	"+ cdtax);
		System.out.println("Tax on cosmetics :	"+ cosmetictax);
		scan.close();
	}

}
